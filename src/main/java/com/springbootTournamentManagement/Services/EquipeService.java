package com.springbootTournamentManagement.Services;

import com.springbootTournamentManagement.Models.Equipe;
import org.springframework.stereotype.Component;

@Component
public class EquipeService {
    public String getNomEquipe(){
        Equipe equipe = new Equipe("Chelsae","UK","Londres");
        return equipe.getNomEquipe();
    }
}
