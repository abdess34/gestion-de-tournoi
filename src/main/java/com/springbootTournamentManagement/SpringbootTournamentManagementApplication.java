package com.springbootTournamentManagement;

import com.springbootTournamentManagement.Services.EquipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootTournamentManagementApplication implements CommandLineRunner {
	@Autowired
	private EquipeService es ;

	public static void main(String[] args) {
		SpringApplication.run(SpringbootTournamentManagementApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String equipe = es.getNomEquipe();
		System.out.println(equipe);
	}
}
